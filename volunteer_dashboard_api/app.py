from flask import Flask
from .users import (
    create_app as user_create_app,
    generate_db as user_generate_db)

app = Flask(__name__)



app = user_create_app(app=app)
db = user_generate_db(app=app)
