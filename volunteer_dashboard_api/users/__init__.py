from flask import Flask, Blueprint
from .config import config as app_config
from .models.database import db

users_blueprint = None


def create_app(*, app, config=None):
    global users_blueprint
    _app = app or Flask(__name__)
    _config = config or app_config
    users = Blueprint('users', __name__)
    _app.config = {**_app.config, **_config}
    # set global blueprint
    users_blueprint = users
    from . import controller  # noqa
    _app.register_blueprint(users)
    return _app


def generate_db(*, app):
    db.init_app(app)
    _db = init_db(db=db)
    return db


def init_db(*, db):
    from .models import users # noqa
    db.create_all()
    return db
